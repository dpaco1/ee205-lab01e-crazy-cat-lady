///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author  David Paco <dpaco@hawaii.edu>
/// @date    13_1_2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
using namespace std;

int main( int argc, char* argv[]) 
{
   char* name = argv[1];
   cout << "Oooh! " << name << " you're so cute!\n" ;
   return 0;
}
